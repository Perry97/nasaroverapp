import React from 'react'
import { useState, useEffect } from 'react';
import RoverImages from './RoverImages';
export default function RoverInfo(props) {

    const [rover, setrover] = useState(props.getRover())
    const [images, setImages] = useState(['noImages'])

    useEffect(() => {
        fetch(`http://localhost:3005/rovers/${rover.name}`)
            .then(res => res.json())
            .then(({latest_photos}) => {
                setImages(latest_photos)

            }).catch(e => console.log('apod Error:', e))

    }, [])


    return (
        <div class="col-sm-6 mb-2">
            <div class="card">
                <div >
                    <p >The {rover.name} rover launched in {rover.launch_date}, it landed on Mars in {rover.landing_date} and the status is {rover.status}</p>
                    <p>The most recent photos are from {rover.max_date}</p>
                    { images[0] === 'noImages'? <> </> : <RoverImages images={images} /> }
                    
                </div>
            </div>
        </div>

    )
}
