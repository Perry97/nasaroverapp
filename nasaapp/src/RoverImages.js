import React from 'react'
import { useState, useEffect } from 'react';
import './RoverImages.css'
import { List } from 'immutable'
import Modal from './Modal'
export default function RoverImages(props) {

    const [roverImages, setrover] = useState(List(props.images));
    const [selectedImage, setSelectedImage] = useState(null)

    return (
        <div className='photos'>
            {roverImages.map((rover) => (
                <div className='roverImage' onClick={() => setSelectedImage(rover.img_src)}><img className="rover-img" src={rover.img_src} alt="W3Schools.com" /></div>

            ))}
            {selectedImage !== null ? <Modal setSelectedImage={setSelectedImage} selectedImage={selectedImage} /> : <></>}
        </div>
    )
}
