import React from 'react'
import './RoverImages.css'

export default function Modal({ selectedImage, setSelectedImage }) {
    return (
        <div className='backDrop' onClick={(e) => setSelectedImage(null)} >
            <img src={selectedImage} ></img>
        </div>
    )
}
