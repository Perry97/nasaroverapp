import './App.css';
import { useState, useEffect } from 'react';
import RoverInfo from './RoverInfo';
import RoverImages from './RoverImages';
import { List } from 'immutable'

function App() {
  const [apod, setApod] = useState('INIT')
  const [roversList, setRoversList] = useState(List([]));
  const [currentRover, setcurrentRover] = useState('noRover')

  useEffect(() => {

    fetch(`http://localhost:3005/rovers`)
      .then(res => res.json())
      .then(rovers => {
        console.log('Rovers', rovers.rovers)
        setRoversList(List(rovers.rovers))
        setcurrentRover(rovers.rovers[0].name)

      }).catch(e => console.log('apod Error:', e))


  }, [])

  const onChangeSelect = (event) => {
    setcurrentRover(event.target.value);
  }

  const getRover = roversList.find((rover) => rover.name === currentRover);

  return (
    <div className="App">

      <header className="App-header">
        <h1>Mars Dashboard</h1>
        <label>Select rover</label>
        <form>
          <select value={currentRover} onChange={onChangeSelect}>
            {roversList.map((rover) => (
              <option value={rover.name}  >{rover.name}</option>
            ))}
          </select>
        </form>
        {currentRover === 'noRover' ? <></> : <RoverInfo getRover={() => getRover} key={currentRover} />}


      </header>
    </div>
  );

}

export default App;